/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmfer
 */
public class SistemaTest {

    public SistemaTest() {
    }

    /**
     * Test of getListaArticulos method, of class Sistema.
     */
    @Test
    public void testGetListaArticulos() {
        Sistema sistema = new Sistema();

        ArrayList<Articulo> miLista = sistema.getListaArticulos();
        ArrayList<Articulo> miOtraLista = new ArrayList<Articulo>();
        assertEquals(miLista, miOtraLista);
    }

    /**
     * Test of agregarArticulo method, of class Sistema.
     */
    @Test
    public void testAgregarArticulo() {
        Sistema sistema = new Sistema();

        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        sistema.agregarArticulo(a);
        int largo = 1;
        assertEquals(sistema.getListaArticulos().size(), largo);
    }

    /**
     * Test of getListaVentas method, of class Sistema.
     */
    @Test
    public void testGetListaVentas() {
        Sistema sistema = new Sistema();

        ArrayList<Venta> miLista = sistema.getListaVentas();
        ArrayList<Venta> miOtraLista = new ArrayList<Venta>();
        assertEquals(miLista, miOtraLista);
    }

    /**
     * Test of setListaVentas method, of class Sistema.
     */
    /**
     * Test of agregarVenta method, of class Sistema.
     */
    @Test
    public void testAgregarVenta() {
        Sistema sistema = new Sistema();

        Venta unaVenta = new Venta(new ArrayList<Articulo>(), "09/08/2019-12:00:33", "Visa", "A-000-1", 1200, 20, true, "5283920-7");
        sistema.agregarVenta(unaVenta);
        int largo = 1;
        assertEquals(sistema.getListaVentas().size(), largo);
    }

    /**
     * Test of articuloMasVendido method, of class Sistema.
     */
    @Test
    public void testArticuloMasVendido() {
        Sistema sistema = new Sistema();

        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-" + sistema.getListaArticulos().size() + 1, "Plastico", "donacion", "Botella");
        sistema.agregarArticulo(a);
        ArrayList<Articulo> lista = new ArrayList<Articulo>();
        lista.add(a);

        Venta unaVenta = new Venta(lista, "09/08/2019-12:00:33", "Visa", "A-000-1", 1200, 20, true, "5283920-7");
        sistema.agregarVenta(unaVenta);

        int[] expResult = {1, 1};
        int[] result = sistema.articuloMasVendido();

        assertArrayEquals(expResult, result);
    }

    /**
     * Test of cantidadDeVentasMes method, of class Sistema.
     */
    @Test
    public void testCantidadDeVentasMes() {
        Sistema sistema = new Sistema();
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-" + sistema.getListaArticulos().size() + 1, "Plastico", "donacion", "Botella");
        sistema.agregarArticulo(a);
        ArrayList<Articulo> lista = new ArrayList<Articulo>();
        lista.add(a);
        Venta unaVenta = new Venta(lista, "09/08/2019-12:00:33", "Visa", "A-000-1", 1200, 20, true, "5283920-7");
        sistema.agregarVenta(unaVenta);
        int[] expResult = {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0};
        int[] result = sistema.cantidadDeVentasMes();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of estimacionBeneficio method, of class Sistema.
     */
    @Test
    public void testEstimacionBeneficio() {
        System.out.println("estimacionBeneficio");
        Sistema sistema = new Sistema();
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-" + sistema.getListaArticulos().size() + 1, "Plastico", "donacion", "Botella");
        sistema.agregarArticulo(a);
        ArrayList<Articulo> lista = new ArrayList<Articulo>();
        lista.add(a);
        Venta unaVenta = new Venta(lista, "09/08/2019-12:00:33", "Visa", "A-000-1", 1200, 20, true, "5283920-7");
        sistema.agregarVenta(unaVenta);

        int expResult = 20;
        int result = sistema.estimacionBeneficio();

        assertEquals(expResult, result);
    }

}
