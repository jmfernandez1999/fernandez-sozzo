/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmfer
 */
public class VentaTest {

    public VentaTest() {
    }

    /**
     * Test of isTrajoEnvase method, of class Venta.
     */
    Venta v = new Venta(new ArrayList<Articulo>(), "09/08/2019-12:00:33", "Visa", "A-000-1", 1200, 20, true, "5283920-7");

    @Test
    public void testIsTrajoEnvase() {
        boolean expResult = true;
        boolean result = v.isTrajoEnvase();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTrajoEnvase method, of class Venta.
     */
    @Test
    public void testSetTrajoEnvase() {
        v.setTrajoEnvase(false);
        boolean trajoEnvase = v.isTrajoEnvase();
        assertEquals(false, trajoEnvase);
    }

    /**
     * Test of getDescuento method, of class Venta.
     */
    @Test
    public void testGetDescuento() {
        int expResult = 20;
        int result = v.getDescuento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescuento method, of class Venta.
     */
    @Test
    public void testSetDescuento() {
        v.setDescuento(30);
        int descuento = v.getDescuento();
        assertEquals(30, descuento);
    }

    /**
     * Test of getPrecioTotal method, of class Venta.
     */
    @Test
    public void testGetPrecioTotal() {

        int expResult = 1200;
        float result = v.getPrecioTotal();
        assertEquals(expResult, result,0);
    }

    /**
     * Test of setPrecioTotal method, of class Venta.
     */
    @Test
    public void testSetPrecioTotal() {
        v.setPrecioTotal(212);
        float precio = v.getPrecioTotal();
        assertEquals(212, precio,0);
    }

    /**
     * Test of getListaArticulos method, of class Venta.
     */
    @Test
    public void testGetListaArticulos() {
        ArrayList<Articulo> miLista = v.getListaArticulos();
        ArrayList<Articulo> miOtraLista = new ArrayList<Articulo>();
        assertEquals(miLista, miOtraLista);

    }

    @Test
    public void testGetidCliente() {
        String expResult = "5283920-7";
        String result = v.getIdCliente();
        assertEquals(expResult, result);

    }

   
    
    /**
     * Test of getFechaVenta method, of class Venta.
     */
    @Test
    public void testGetFechaVenta() {

        String expResult = "09/08/2019-12:00:33";
        String result = v.getFechaVenta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFechaVenta method, of class Venta.
     */
    @Test
    public void testSetFechaVenta() {
        v.setFechaVenta("22/02/2019-12:33:21 PM");
        String fecha = v.getFechaVenta();
        assertEquals("22/02/2019-12:33:21 PM", fecha);
    }

    /**
     * Test of getFormaDePago method, of class Venta.
     */
    @Test
    public void testGetFormaDePago() {
        String expResult = "Visa";
        String result = v.getFormaDePago();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFormaDePago method, of class Venta.
     */
    @Test
    public void testSetFormaDePago() {
        v.setFormaDePago("Maestro");
        String formaPago = v.getFormaDePago();
        assertEquals("Maestro", formaPago);
    }

    /**
     * Test of getNumeroDeSerie method, of class Venta.
     */
    @Test
    public void testGetNumeroDeSerie() {

        String expResult = "A-000-1";
        String result = v.getNumeroDeSerie();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNumeroDeSerie method, of class Venta.
     */
    @Test
    public void testSetNumeroDeSerie() {
        v.setNumeroDeSerie("A0000-12");
        String numeroSerie = v.getNumeroDeSerie();
        assertEquals("A0000-12", numeroSerie);
    }

}
