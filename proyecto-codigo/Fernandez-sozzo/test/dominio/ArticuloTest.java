/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jmfer
 */
public class ArticuloTest {

    public ArticuloTest() {
    }

    /**
     * Test of getNombre method, of class Articulo.
     */
    @Test
    public void testGetNombre() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        String expResult = "Botella de 3 litros";
        String result = a.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Articulo.
     */
    @Test
    public void testSetNombre() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setNombre("caja de carton");
        String miNombre = a.getNombre();
        assertEquals("caja de carton", miNombre);
    }

    /**
     * Test of getTipo method, of class Articulo.
     */
    @Test
    public void testGetTipo() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        String expResult = "Botella";
        String result = a.getTipo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipo method, of class Articulo.
     */
    @Test
    public void testSetTipo() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setTipo("Caja");
        String miTipo = a.getTipo();
        assertEquals("Caja", miTipo);
    }

    /**
     * Test of getPrecio method, of class Articulo.
     */
    @Test
    public void testGetPrecio() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        float expResult = 123;
        float result = a.getPrecio();
        assertEquals(expResult, result,0);
    }

    /**
     * Test of setPrecio method, of class Articulo.
     */
    @Test
    public void testSetPrecio() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setPrecio(1233);
        float miPrecio = a.getPrecio();
        assertEquals(1233, miPrecio,0);
    }
    
    

    /**
     * Test of getMaterial method, of class Articulo.
     */
    @Test
    public void testGetMaterial() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        String expResult = "Plastico";
        String result = a.getMaterial();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMaterial method, of class Articulo.
     */
    @Test
    public void testSetMaterial() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setMaterial("Carton");
        String miMaterial = a.getMaterial();
        assertEquals("Carton", miMaterial);

    }

    /**
     * Test of getCodigo method, of class Articulo.
     */
    @Test
    public void testGetCodigo() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        String expResult = "ART-123";
        String result = a.getCodigo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCodigo method, of class Articulo.
     */
    @Test
    public void testSetCodigo() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setCodigo("ART-12345");
        String miCodigo = a.getCodigo();
        assertEquals("ART-12345", miCodigo);
    }

    /**
     * Test of getOrigenMateriaPrima method, of class Articulo.
     */
    @Test
    public void testGetOrigenMateriaPrima() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");
        String expResult = "donacion";
        String result = a.getOrigenMateriaPrima();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrigenMateriaPrima method, of class Articulo.
     */
    @Test
    public void testSetOrigenMateriaPrima() {
        Articulo a = new Articulo("Botella de 3 litros", 123, "ART-123", "Plastico", "donacion", "Botella");

        a.setOrigenMateriaPrima("No se sabe");
        String miOrigen = a.getOrigenMateriaPrima();
        assertEquals("No se sabe", miOrigen);
    }

    
     
}
