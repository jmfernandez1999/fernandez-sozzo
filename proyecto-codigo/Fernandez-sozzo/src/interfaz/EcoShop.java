/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import dominio.Sistema;
import java.io.FileNotFoundException;

/**
 *
 * @author jmfer
 */
public class EcoShop {

    public static void main(String[] args) throws FileNotFoundException {
        Sistema s = new Sistema();
        s.abrirArticulos();
        s.abrirVentas();
        Home v = new Home(s);
        v.setVisible(true);

    }

}
