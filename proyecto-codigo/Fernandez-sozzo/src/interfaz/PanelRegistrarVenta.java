/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import dominio.Articulo;
import dominio.Sistema;
import dominio.Venta;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 *
 * @author jmfer
 */
public class PanelRegistrarVenta extends javax.swing.JPanel {

    private Sistema s;
    Venta v;
    ArrayList<Articulo> listaCarrito = new ArrayList<Articulo>();
    float total;
    int descuento;
    float precio = 0;

    boolean presentaEnvase;
    boolean preventa = false;

    public PanelRegistrarVenta(Sistema sis) throws FileNotFoundException {
        s = sis;
        initComponents();

        ButtonGroup grupo = new ButtonGroup();
        this.tipoEnvaseCB.setVisible(false);
        this.calendarioDC.setVisible(false);
        this.alertaLbl.setVisible(false);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        formaDePagoBtnGroup = new javax.swing.ButtonGroup();
        tituloLbl = new javax.swing.JLabel();
        contadoBtn = new javax.swing.JRadioButton();
        visaBtn = new javax.swing.JRadioButton();
        mastercardBtn = new javax.swing.JRadioButton();
        americanBtn = new javax.swing.JRadioButton();
        cabalBtn = new javax.swing.JRadioButton();
        maestroBtn = new javax.swing.JRadioButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jlistCarrito = new javax.swing.JList<>();
        formaDePagoLbl = new javax.swing.JLabel();
        totalLbl = new javax.swing.JLabel();
        precioTotal = new javax.swing.JLabel();
        confirmarBtn = new javax.swing.JButton();
        agregarBtn = new javax.swing.JButton();
        eliminarBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jlistProductos = new javax.swing.JList<>();
        jLabel5 = new javax.swing.JLabel();
        checkBoxEnvase = new javax.swing.JRadioButton();
        tipoEnvaseCB = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        txtCI = new javax.swing.JTextField();
        checkBoxPreVenta = new javax.swing.JRadioButton();
        calendarioDC = new rojeru_san.componentes.RSDateChooser();
        jLabel2 = new javax.swing.JLabel();
        labelDescuento = new javax.swing.JLabel();
        alertaLbl = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        tituloLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        tituloLbl.setText("Registro de Venta");

        formaDePagoBtnGroup.add(contadoBtn);
        contadoBtn.setText("Contado");
        contadoBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contadoBtnActionPerformed(evt);
            }
        });

        formaDePagoBtnGroup.add(visaBtn);
        visaBtn.setText("Visa");
        visaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visaBtnActionPerformed(evt);
            }
        });

        formaDePagoBtnGroup.add(mastercardBtn);
        mastercardBtn.setText("MasterCard");

        formaDePagoBtnGroup.add(americanBtn);
        americanBtn.setText("American Express");
        americanBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                americanBtnActionPerformed(evt);
            }
        });

        formaDePagoBtnGroup.add(cabalBtn);
        cabalBtn.setText("Cabal");

        formaDePagoBtnGroup.add(maestroBtn);
        maestroBtn.setText("Maestro");
        maestroBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maestroBtnActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jlistCarrito);

        formaDePagoLbl.setText("Forma de pago: ");

        totalLbl.setText("Total: ");

        precioTotal.setText("0");

        confirmarBtn.setText("Confirmar Venta");
        confirmarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarBtnActionPerformed(evt);
            }
        });

        agregarBtn.setText("Agregar");
        agregarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarBtnActionPerformed(evt);
            }
        });

        eliminarBtn.setText("Eliminar");
        eliminarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarBtnActionPerformed(evt);
            }
        });

        jLabel4.setText("Carro:");

        jScrollPane2.setViewportView(jlistProductos);

        jLabel5.setText("Productos");

        checkBoxEnvase.setBackground(new java.awt.Color(255, 255, 255));
        checkBoxEnvase.setText("Presenta Envase");
        checkBoxEnvase.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        checkBoxEnvase.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                checkBoxEnvaseStateChanged(evt);
            }
        });
        checkBoxEnvase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBoxEnvaseActionPerformed(evt);
            }
        });

        tipoEnvaseCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione uno", "Botella", "Caja", "Recipiente", "Bolsa" }));
        tipoEnvaseCB.setToolTipText("");
        tipoEnvaseCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tipoEnvaseCBItemStateChanged(evt);
            }
        });
        tipoEnvaseCB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipoEnvaseCBActionPerformed(evt);
            }
        });
        tipoEnvaseCB.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tipoEnvaseCBPropertyChange(evt);
            }
        });

        jLabel1.setText("C.I ");

        checkBoxPreVenta.setBackground(new java.awt.Color(255, 255, 255));
        checkBoxPreVenta.setText("Preventa");
        checkBoxPreVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkBoxPreVentaActionPerformed(evt);
            }
        });

        calendarioDC.setColorBackground(new java.awt.Color(51, 118, 18));
        calendarioDC.setColorButtonHover(new java.awt.Color(139, 222, 139));
        calendarioDC.setColorForeground(new java.awt.Color(51, 153, 0));

        jLabel2.setText("Descuento:");

        labelDescuento.setText("0");

        alertaLbl.setForeground(new java.awt.Color(255, 0, 51));
        alertaLbl.setText("La fecha tiene que ser posterior a la fecha actual, si no se utilizara la fecha atual");

        jLabel3.setForeground(new java.awt.Color(255, 0, 51));
        jLabel3.setText("Ingrese la cedula sin guion");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(agregarBtn)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 18, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(eliminarBtn)
                        .addGap(104, 104, 104)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(totalLbl)
                            .addComponent(jLabel2))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelDescuento)
                            .addComponent(precioTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38)
                        .addComponent(confirmarBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 22, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkBoxEnvase)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(formaDePagoLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(tipoEnvaseCB, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCI, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(contadoBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(mastercardBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(visaBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(maestroBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cabalBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(americanBtn)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(312, 312, 312)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(checkBoxPreVenta)
                    .addComponent(tituloLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addComponent(calendarioDC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(alertaLbl)
                .addGap(28, 28, 28))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(tituloLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(alertaLbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(checkBoxPreVenta)
                    .addComponent(calendarioDC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(formaDePagoLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(contadoBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mastercardBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(visaBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(americanBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cabalBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(maestroBtn)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtCI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(checkBoxEnvase)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tipoEnvaseCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(confirmarBtn)
                                    .addComponent(agregarBtn)
                                    .addComponent(eliminarBtn)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(labelDescuento))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(totalLbl)
                                    .addComponent(precioTotal))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void contadoBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contadoBtnActionPerformed
    }//GEN-LAST:event_contadoBtnActionPerformed

    private void visaBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visaBtnActionPerformed
     }//GEN-LAST:event_visaBtnActionPerformed

    private void americanBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_americanBtnActionPerformed
    }//GEN-LAST:event_americanBtnActionPerformed

    private void maestroBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maestroBtnActionPerformed
     }//GEN-LAST:event_maestroBtnActionPerformed

    private void confirmarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarBtnActionPerformed
        try {
            crearVenta();
            JOptionPane.showMessageDialog(null, "Se finalizo la venta", "", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(PanelRegistrarVenta.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_confirmarBtnActionPerformed

    private void agregarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarBtnActionPerformed
        precio = 0;
        Articulo a = s.getListaArticulos().get(this.jlistProductos.getSelectedIndex());
        listaCarrito.add(a);

        String[] objetosParaVenta = new String[listaCarrito.size()];

        for (int i = 0; i < listaCarrito.size(); i++) {
            objetosParaVenta[i] = listaCarrito.get(i).getNombre() + " $" + listaCarrito.get(i).getPrecio();
            precio += listaCarrito.get(i).getPrecio();
        }
        this.precioTotal.setText("$" + (precio - descuento));
        this.jlistCarrito.setListData(objetosParaVenta);

    }//GEN-LAST:event_agregarBtnActionPerformed

    private void checkBoxEnvaseStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_checkBoxEnvaseStateChanged


    }//GEN-LAST:event_checkBoxEnvaseStateChanged

    private void checkBoxEnvaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBoxEnvaseActionPerformed
        if (this.checkBoxEnvase.isSelected()) {
            this.tipoEnvaseCB.setVisible(true);
            presentaEnvase = true;
            String tipoEnvase = this.tipoEnvaseCB.getSelectedItem().toString();
            switch (tipoEnvase) {
                case "Botella":
                    descuento = 100;

                    break;
                case "Caja":
                    descuento = 70;

                    break;
                case "Recipiente":
                    descuento = 85;

                    break;
                case "Bolsa":
                    descuento = 40;

                    break;
            }
            this.precioTotal.setText("$" + (precio - descuento));
            this.labelDescuento.setText("$" + descuento);
        } else {

            this.tipoEnvaseCB.setVisible(false);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_checkBoxEnvaseActionPerformed

    private void tipoEnvaseCBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipoEnvaseCBActionPerformed
        String tipoEnvase = this.tipoEnvaseCB.getSelectedItem().toString();
        switch (tipoEnvase) {
            case "Botella":
                descuento = 100;

                break;
            case "Caja":
                descuento = 70;

                break;
            case "Recipiente":
                descuento = 85;

                break;
            case "Bolsa":
                descuento = 40;

                break;
        }
        this.precioTotal.setText("$" + (precio - descuento));
        this.labelDescuento.setText("$" + descuento);

    }//GEN-LAST:event_tipoEnvaseCBActionPerformed

    private void checkBoxPreVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkBoxPreVentaActionPerformed
        // TODO add your handling code here:
        if (this.checkBoxPreVenta.isSelected()) {
            this.calendarioDC.setVisible(true);
            this.alertaLbl.setVisible(true);
            preventa = true;
        } else {
            this.calendarioDC.setVisible(false);
            this.alertaLbl.setVisible(false);
            preventa = false;
        }
    }//GEN-LAST:event_checkBoxPreVentaActionPerformed

    private void tipoEnvaseCBPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tipoEnvaseCBPropertyChange

    }//GEN-LAST:event_tipoEnvaseCBPropertyChange

    private void tipoEnvaseCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tipoEnvaseCBItemStateChanged
        this.precioTotal.setText("$" + (precio - descuento));
        this.labelDescuento.setText("$" + descuento);        // TODO add your handling code here:
    }//GEN-LAST:event_tipoEnvaseCBItemStateChanged

    private void eliminarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarBtnActionPerformed
        // TODO add your handling code here:
        listaCarrito.remove(this.jlistCarrito.getSelectedIndex());
        String[] objetosParaVenta = new String[listaCarrito.size()];
        precio = 0;
        for (int i = 0; i < listaCarrito.size(); i++) {
            objetosParaVenta[i] = listaCarrito.get(i).getNombre() + " $" + listaCarrito.get(i).getPrecio();
            precio += listaCarrito.get(i).getPrecio();
        }
        this.precioTotal.setText("$" + (precio - descuento));
        this.jlistCarrito.setListData(objetosParaVenta);

    }//GEN-LAST:event_eliminarBtnActionPerformed
    public void crearVenta() throws IOException {
        String ci = this.txtCI.getText();
        String metodoDePago = "";
        if (this.americanBtn.isSelected()) {
            metodoDePago = "American Express";
        } else if (this.contadoBtn.isSelected()) {
            metodoDePago = "Contado";
        } else if (this.visaBtn.isSelected()) {
            metodoDePago = "Visa";
        } else if (this.cabalBtn.isSelected()) {
            metodoDePago = "Cabal";
        } else if (this.mastercardBtn.isSelected()) {
            metodoDePago = "MasterCard";
        } else if (this.maestroBtn.isSelected()) {
            metodoDePago = "Maestro";
        }
        total = 0;
        for (int i = 0; i < this.listaCarrito.size(); i++) {
            total += listaCarrito.get(i).getPrecio();
        }
        Date today = new Date();
        String fechaDeVenta;
        //formatting date in Java using SimpleDateFormat
        if (preventa) {
            if (calendarioDC.getDatoFecha() == null) {
                SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
                DATE_FORMAT = new SimpleDateFormat("dd/MM/yy");
                fechaDeVenta = DATE_FORMAT.format(today);
            } else {
                Date fecha = calendarioDC.getDatoFecha();
                if (fecha.after(today)) {
                    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
                    DATE_FORMAT = new SimpleDateFormat("dd/MM/yy");
                    fechaDeVenta = DATE_FORMAT.format(fecha);
                } else {
                    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
                    DATE_FORMAT = new SimpleDateFormat("dd/MM/yy");
                    fechaDeVenta = DATE_FORMAT.format(today);
                    JOptionPane.showMessageDialog(this, "La fecha ingresada es anterior a la fecha actual se utilizara la fecha actual como fecha", "Atención", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } else {
            SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
            DATE_FORMAT = new SimpleDateFormat("dd/MM/yy-HH:mm:SS");
            fechaDeVenta = DATE_FORMAT.format(today);
        }
        if (listaCarrito.size() > 0 && metodoDePago != "" && !ci.equals("")) {
            if (descuento > total) {
                total = 0;
            }
            v = new Venta(listaCarrito, fechaDeVenta, metodoDePago, "nro", total, descuento, presentaEnvase, ci);
            s.agregarVenta(v);
            s.guardarVentas();
            generarPdf();
        } else {

            JOptionPane.showMessageDialog(this, listaCarrito.size() == 0 ? "Debe seleccionar algún producto" : metodoDePago.equals("") ? "Debe seleccionar un método de pago" : "Debe ingresar C.I de cliente", "Alerta", WARNING_MESSAGE);

        }

    }

    public void actualizarListaArticulos() {
        String[] nombres = new String[s.getListaArticulos().size()];
        for (int i = 0; i < s.getListaArticulos().size(); i++) {
            nombres[i] = s.getListaArticulos().get(i).getNombre();

        }
        this.jlistProductos.setListData(nombres);
    }

    private void generarPdf() throws IOException {
        //Saving the document
        //Creating PDF document object
        PDDocument document = new PDDocument();

        //Creating a blank page 
        PDPage blankPage = new PDPage();

        //Adding the blank page to the document
        document.addPage(blankPage);
        PDPageContentStream stream = new PDPageContentStream(document, blankPage);
        //Saving the document
        int marginTop = 30;
        PDFont font = PDType1Font.HELVETICA_BOLD; // Or whatever font you want.
        float titleWidth = font.getStringWidth("EcoShop S.A") / 1000 * 20;
        float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * 16;

        stream.beginText();
        stream.setFont(font, 20);
        stream.moveTextPositionByAmount((blankPage.getMediaBox().getWidth() - titleWidth) / 5, blankPage.getMediaBox().getHeight() - marginTop - titleHeight);
        stream.drawString("EcoShop S.A");
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.moveTextPositionByAmount((blankPage.getMediaBox().getWidth() - titleWidth) / 5, blankPage.getMediaBox().getHeight() - marginTop - titleHeight - 20);
        stream.drawString("Av 18 de Julio - 1199");
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 18);
        stream.newLineAtOffset((blankPage.getMediaBox().getWidth() - titleWidth) / 2, 600);

        stream.drawString("Consumo Final");

        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 18);
        stream.newLineAtOffset((blankPage.getMediaBox().getWidth() - titleWidth) / 2, 595);
        stream.drawString("____________");
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.newLineAtOffset(475, 750);
        stream.drawString("e-TICKET");
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.newLineAtOffset(475, 740);
        stream.drawString(v.getFechaVenta());
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.newLineAtOffset(475, 730);
        stream.drawString("C.I: " + v.getIdCliente());
        stream.endText();

        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.newLineAtOffset(475, 720);
        stream.drawString(v.getFormaDePago());
        stream.endText();

        int dist = 550;
        for (int i = 0; i < v.getListaArticulos().size(); i++) {
            stream.beginText();
            stream.setFont(PDType1Font.HELVETICA, 10);
            stream.newLineAtOffset((blankPage.getMediaBox().getWidth() - titleWidth) / 6, dist);
            stream.drawString("- " + v.getListaArticulos().get(i).getNombre() + "  " + "................................................................................................................. " + "$" + listaCarrito.get(i).getPrecio());
            dist -= 10;
            stream.endText();

        }
        if (presentaEnvase) {
            stream.beginText();
            stream.setFont(PDType1Font.HELVETICA, 10);
            stream.newLineAtOffset((blankPage.getMediaBox().getWidth() - titleWidth) - 90, dist - 10);
            stream.drawString("Descuento por envase $ " + descuento);
            stream.endText();
        }
        stream.beginText();
        stream.setFont(PDType1Font.HELVETICA, 10);
        stream.newLineAtOffset((blankPage.getMediaBox().getWidth() - titleWidth) - 50, dist - 20);
        stream.drawString("Total: $ " + (total - descuento < 0 ? 0 : total - descuento));
        stream.endText();

        stream.close();
        int nroFactura = s.getListaVentas().size();

        String lugar = System.getProperty("user.home") + "/Desktop/";
        FileOutputStream fileOut = new FileOutputStream(lugar + "Factura_" + nroFactura + ".pdf");
        document.save(fileOut);
        Runtime.getRuntime().exec("cmd /c start " + System.getProperty("user.home") + "/Desktop/Factura_" + nroFactura + ".pdf");
        System.out.println("PDF created");
        //Closing the document

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregarBtn;
    private javax.swing.JLabel alertaLbl;
    private javax.swing.JRadioButton americanBtn;
    private javax.swing.JRadioButton cabalBtn;
    private rojeru_san.componentes.RSDateChooser calendarioDC;
    private javax.swing.JRadioButton checkBoxEnvase;
    private javax.swing.JRadioButton checkBoxPreVenta;
    private javax.swing.JButton confirmarBtn;
    private javax.swing.JRadioButton contadoBtn;
    private javax.swing.JButton eliminarBtn;
    private javax.swing.ButtonGroup formaDePagoBtnGroup;
    private javax.swing.JLabel formaDePagoLbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> jlistCarrito;
    private javax.swing.JList<String> jlistProductos;
    private javax.swing.JLabel labelDescuento;
    private javax.swing.JRadioButton maestroBtn;
    private javax.swing.JRadioButton mastercardBtn;
    private javax.swing.JLabel precioTotal;
    private javax.swing.JComboBox<String> tipoEnvaseCB;
    private javax.swing.JLabel tituloLbl;
    private javax.swing.JLabel totalLbl;
    private javax.swing.JTextField txtCI;
    private javax.swing.JRadioButton visaBtn;
    // End of variables declaration//GEN-END:variables

}
