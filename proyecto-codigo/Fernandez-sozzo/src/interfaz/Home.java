/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import dominio.Sistema;
import java.awt.Color;
import java.io.FileNotFoundException;

/**
 *
 * @author Joaquin
 */
public class Home extends javax.swing.JFrame {

    /**
     * Creates new form Home
     */
    private Sistema s;
    PanelRegistrarProducto panelRegistrarArticulo;
    PanelPuntosDeVenta panelPuntosDeVenta = new PanelPuntosDeVenta();
    PanelReportesEstadisticas panelReportes;
    PanelRegistrarVenta panelRegistrarVenta;
    PanelVerEnvaces panelVerEnvaces;
    PanelInicial panelInicio = new PanelInicial();
    PanelListadoDeClientes panelClientes;

    public Home(Sistema sis) throws FileNotFoundException {
        s = sis;
        panelReportes = new PanelReportesEstadisticas(s);

        panelRegistrarArticulo = new PanelRegistrarProducto(s);
        panelRegistrarVenta = new PanelRegistrarVenta(s);
        panelVerEnvaces = new PanelVerEnvaces();
        panelClientes = new PanelListadoDeClientes(s);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        panelInicio.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelInicio);
        panelRegistrarArticulo.setVisible(true);

        panelRegistrarArticulo.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelRegistrarArticulo);
        panelRegistrarArticulo.setVisible(false);

        panelPuntosDeVenta.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelPuntosDeVenta);
        panelPuntosDeVenta.setVisible(false);

        panelRegistrarVenta.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelRegistrarVenta);
        panelRegistrarVenta.setVisible(false);

        panelReportes.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelReportes);
        panelReportes.setVisible(false);

        panelVerEnvaces.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelVerEnvaces);
        panelVerEnvaces.setVisible(false);

        panelClientes.setBounds(0, 0, 870, 461);
        this.panelCentral.add(panelClientes);
        panelClientes.setVisible(false);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backgroundPanel = new javax.swing.JPanel();
        sidepanePanel = new javax.swing.JPanel();
        inicioPanel = new javax.swing.JPanel();
        inicioIconoLbl = new javax.swing.JLabel();
        inicioTxtLbl = new javax.swing.JLabel();
        tituloSpLbl = new javax.swing.JLabel();
        registrarArticuloPanel = new javax.swing.JPanel();
        registrarArticuloIconoLbl = new javax.swing.JLabel();
        registrarArticuloTxtLbl = new javax.swing.JLabel();
        puntoDeVentaPanel = new javax.swing.JPanel();
        puntoDeVentaLbl = new javax.swing.JLabel();
        puntoDeVentaTxtLbl = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        registroDeVentaPanel = new javax.swing.JPanel();
        ventasLbl = new javax.swing.JLabel();
        ventasTxtLbl = new javax.swing.JLabel();
        reportesEstadisticasPanel = new javax.swing.JPanel();
        reporteLbl = new javax.swing.JLabel();
        reportesTxtLbl = new javax.swing.JLabel();
        verEnvasesPanel = new javax.swing.JPanel();
        envasesLbl = new javax.swing.JLabel();
        envasesTxtLbl = new javax.swing.JLabel();
        listaClientesPanel = new javax.swing.JPanel();
        listaClientesLbl = new javax.swing.JLabel();
        listaClientesTxtLbl = new javax.swing.JLabel();
        panelCentral = new javax.swing.JPanel();
        tituloPanel = new javax.swing.JPanel();
        tituloLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setLocationByPlatform(true);

        backgroundPanel.setBackground(new java.awt.Color(255, 255, 255));

        sidepanePanel.setBackground(new java.awt.Color(0, 102, 51));
        sidepanePanel.setForeground(new java.awt.Color(0, 102, 51));
        sidepanePanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        inicioPanel.setBackground(new java.awt.Color(51, 153, 0));
        inicioPanel.setForeground(new java.awt.Color(51, 153, 0));
        inicioPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inicioPanelMouseClicked(evt);
            }
        });

        inicioIconoLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_home_32px.png"))); // NOI18N
        inicioIconoLbl.setIconTextGap(20);

        inicioTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        inicioTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        inicioTxtLbl.setText("Inicio");

        javax.swing.GroupLayout inicioPanelLayout = new javax.swing.GroupLayout(inicioPanel);
        inicioPanel.setLayout(inicioPanelLayout);
        inicioPanelLayout.setHorizontalGroup(
            inicioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inicioPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(inicioIconoLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inicioTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        inicioPanelLayout.setVerticalGroup(
            inicioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inicioPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(inicioTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inicioPanelLayout.createSequentialGroup()
                .addComponent(inicioIconoLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(inicioPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 260, 40));

        tituloSpLbl.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        tituloSpLbl.setForeground(new java.awt.Color(255, 255, 255));
        tituloSpLbl.setText("ECO SHOP");
        sidepanePanel.add(tituloSpLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, 100, 40));

        registrarArticuloPanel.setBackground(new java.awt.Color(51, 118, 18));
        registrarArticuloPanel.setForeground(new java.awt.Color(51, 153, 0));
        registrarArticuloPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                registrarArticuloPanelMouseClicked(evt);
            }
        });

        registrarArticuloIconoLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_add_32px.png"))); // NOI18N
        registrarArticuloIconoLbl.setIconTextGap(20);

        registrarArticuloTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        registrarArticuloTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        registrarArticuloTxtLbl.setText("Registrar Articulo");

        javax.swing.GroupLayout registrarArticuloPanelLayout = new javax.swing.GroupLayout(registrarArticuloPanel);
        registrarArticuloPanel.setLayout(registrarArticuloPanelLayout);
        registrarArticuloPanelLayout.setHorizontalGroup(
            registrarArticuloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registrarArticuloPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(registrarArticuloIconoLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(registrarArticuloTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        registrarArticuloPanelLayout.setVerticalGroup(
            registrarArticuloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registrarArticuloPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(registrarArticuloTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registrarArticuloPanelLayout.createSequentialGroup()
                .addComponent(registrarArticuloIconoLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(registrarArticuloPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, -1, 40));

        puntoDeVentaPanel.setBackground(new java.awt.Color(51, 118, 18));
        puntoDeVentaPanel.setForeground(new java.awt.Color(51, 153, 0));
        puntoDeVentaPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                puntoDeVentaPanelMouseClicked(evt);
            }
        });

        puntoDeVentaLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_map_marker_32px.png"))); // NOI18N
        puntoDeVentaLbl.setIconTextGap(20);

        puntoDeVentaTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        puntoDeVentaTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        puntoDeVentaTxtLbl.setText("Puntos De Venta");

        javax.swing.GroupLayout puntoDeVentaPanelLayout = new javax.swing.GroupLayout(puntoDeVentaPanel);
        puntoDeVentaPanel.setLayout(puntoDeVentaPanelLayout);
        puntoDeVentaPanelLayout.setHorizontalGroup(
            puntoDeVentaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(puntoDeVentaPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(puntoDeVentaLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(puntoDeVentaTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        puntoDeVentaPanelLayout.setVerticalGroup(
            puntoDeVentaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, puntoDeVentaPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(puntoDeVentaTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, puntoDeVentaPanelLayout.createSequentialGroup()
                .addComponent(puntoDeVentaLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(puntoDeVentaPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, -1, 40));
        sidepanePanel.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 220, 10));

        registroDeVentaPanel.setBackground(new java.awt.Color(51, 118, 18));
        registroDeVentaPanel.setForeground(new java.awt.Color(51, 153, 0));
        registroDeVentaPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                registroDeVentaPanelMouseClicked(evt);
            }
        });

        ventasLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_receive_cash_32px.png"))); // NOI18N
        ventasLbl.setIconTextGap(20);

        ventasTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ventasTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        ventasTxtLbl.setText("Registro de Venta");

        javax.swing.GroupLayout registroDeVentaPanelLayout = new javax.swing.GroupLayout(registroDeVentaPanel);
        registroDeVentaPanel.setLayout(registroDeVentaPanelLayout);
        registroDeVentaPanelLayout.setHorizontalGroup(
            registroDeVentaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registroDeVentaPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(ventasLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ventasTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        registroDeVentaPanelLayout.setVerticalGroup(
            registroDeVentaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroDeVentaPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ventasTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registroDeVentaPanelLayout.createSequentialGroup()
                .addComponent(ventasLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(registroDeVentaPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, -1, 40));

        reportesEstadisticasPanel.setBackground(new java.awt.Color(51, 118, 18));
        reportesEstadisticasPanel.setForeground(new java.awt.Color(51, 153, 0));
        reportesEstadisticasPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reportesEstadisticasPanelMouseClicked(evt);
            }
        });

        reporteLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/graph_32px.png"))); // NOI18N
        reporteLbl.setIconTextGap(20);

        reportesTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        reportesTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        reportesTxtLbl.setText("Reportes de estadísticas");

        javax.swing.GroupLayout reportesEstadisticasPanelLayout = new javax.swing.GroupLayout(reportesEstadisticasPanel);
        reportesEstadisticasPanel.setLayout(reportesEstadisticasPanelLayout);
        reportesEstadisticasPanelLayout.setHorizontalGroup(
            reportesEstadisticasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(reportesEstadisticasPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(reporteLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(reportesTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        reportesEstadisticasPanelLayout.setVerticalGroup(
            reportesEstadisticasPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, reportesEstadisticasPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(reportesTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, reportesEstadisticasPanelLayout.createSequentialGroup()
                .addComponent(reporteLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(reportesEstadisticasPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 300, -1, 40));

        verEnvasesPanel.setBackground(new java.awt.Color(51, 118, 18));
        verEnvasesPanel.setForeground(new java.awt.Color(51, 153, 0));
        verEnvasesPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verEnvasesPanelMouseClicked(evt);
            }
        });

        envasesLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_bottle_of_water_32px.png"))); // NOI18N
        envasesLbl.setIconTextGap(20);

        envasesTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        envasesTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        envasesTxtLbl.setText("Envases");

        javax.swing.GroupLayout verEnvasesPanelLayout = new javax.swing.GroupLayout(verEnvasesPanel);
        verEnvasesPanel.setLayout(verEnvasesPanelLayout);
        verEnvasesPanelLayout.setHorizontalGroup(
            verEnvasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(verEnvasesPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(envasesLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(envasesTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        verEnvasesPanelLayout.setVerticalGroup(
            verEnvasesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, verEnvasesPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(envasesTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, verEnvasesPanelLayout.createSequentialGroup()
                .addComponent(envasesLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(verEnvasesPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 260, 260, 40));

        listaClientesPanel.setBackground(new java.awt.Color(51, 118, 18));
        listaClientesPanel.setForeground(new java.awt.Color(51, 153, 0));
        listaClientesPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaClientesPanelMouseClicked(evt);
            }
        });

        listaClientesLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/icons8_user_groups_32px.png"))); // NOI18N
        listaClientesLbl.setIconTextGap(20);

        listaClientesTxtLbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        listaClientesTxtLbl.setForeground(new java.awt.Color(255, 255, 255));
        listaClientesTxtLbl.setText("Listado de Clientes");

        javax.swing.GroupLayout listaClientesPanelLayout = new javax.swing.GroupLayout(listaClientesPanel);
        listaClientesPanel.setLayout(listaClientesPanelLayout);
        listaClientesPanelLayout.setHorizontalGroup(
            listaClientesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(listaClientesPanelLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(listaClientesLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(listaClientesTxtLbl, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                .addGap(14, 14, 14))
        );
        listaClientesPanelLayout.setVerticalGroup(
            listaClientesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, listaClientesPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(listaClientesTxtLbl)
                .addGap(23, 23, 23))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, listaClientesPanelLayout.createSequentialGroup()
                .addComponent(listaClientesLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        sidepanePanel.add(listaClientesPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 260, 40));

        panelCentral.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout panelCentralLayout = new javax.swing.GroupLayout(panelCentral);
        panelCentral.setLayout(panelCentralLayout);
        panelCentralLayout.setHorizontalGroup(
            panelCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 870, Short.MAX_VALUE)
        );
        panelCentralLayout.setVerticalGroup(
            panelCentralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 461, Short.MAX_VALUE)
        );

        tituloPanel.setBackground(new java.awt.Color(139, 222, 139));
        tituloPanel.setForeground(new java.awt.Color(0, 213, 0));

        tituloLbl.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        tituloLbl.setText("Bienvendio");

        javax.swing.GroupLayout tituloPanelLayout = new javax.swing.GroupLayout(tituloPanel);
        tituloPanel.setLayout(tituloPanelLayout);
        tituloPanelLayout.setHorizontalGroup(
            tituloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tituloPanelLayout.createSequentialGroup()
                .addContainerGap(319, Short.MAX_VALUE)
                .addComponent(tituloLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(303, 303, 303))
        );
        tituloPanelLayout.setVerticalGroup(
            tituloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tituloPanelLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(tituloLbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(484, 484, 484))
        );

        javax.swing.GroupLayout backgroundPanelLayout = new javax.swing.GroupLayout(backgroundPanel);
        backgroundPanel.setLayout(backgroundPanelLayout);
        backgroundPanelLayout.setHorizontalGroup(
            backgroundPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(sidepanePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(backgroundPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tituloPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelCentral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        backgroundPanelLayout.setVerticalGroup(
            backgroundPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sidepanePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 610, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(backgroundPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(tituloPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelCentral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(backgroundPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(backgroundPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 599, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void puntoDeVentaPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_puntoDeVentaPanelMouseClicked
        // TODO add your handling code here:
        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 153, 0));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51,118, 18));
        panelRegistrarArticulo.setVisible(false);
        panelVerEnvaces.setVisible(false);
        panelInicio.setVisible(false);
        this.tituloLbl.setText("EcoShop");
        panelPuntosDeVenta.setVisible(true);
        panelClientes.setVisible(false);
        ;

    }//GEN-LAST:event_puntoDeVentaPanelMouseClicked

    private void registrarArticuloPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_registrarArticuloPanelMouseClicked
        // TODO add your handling code here:
        this.tituloLbl.setText("EcoShop");

        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 153, 0));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51,118, 18));
        panelInicio.setVisible(false);

        panelVerEnvaces.setVisible(false);
        panelPuntosDeVenta.setVisible(false);
        panelRegistrarVenta.setVisible(false);
        panelRegistrarArticulo.setVisible(true);
        panelClientes.setVisible(false);


    }//GEN-LAST:event_registrarArticuloPanelMouseClicked

    private void reportesEstadisticasPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reportesEstadisticasPanelMouseClicked
        this.tituloLbl.setText("EcoShop");

        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 153, 0));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51,118, 18));
        panelRegistrarArticulo.setVisible(false);
        panelRegistrarVenta.setVisible(false);
        panelInicio.setVisible(false);

        panelPuntosDeVenta.setVisible(false);
        panelVerEnvaces.setVisible(false);
        panelClientes.setVisible(false);
        panelReportes.setVisible(true);
        panelReportes.actualizarTabla();

    }//GEN-LAST:event_reportesEstadisticasPanelMouseClicked

    private void registroDeVentaPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_registroDeVentaPanelMouseClicked
        this.tituloLbl.setText("EcoShop");

        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 153, 0));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51,118, 18));
        panelRegistrarArticulo.setVisible(false);
        panelPuntosDeVenta.setVisible(false);
        panelReportes.setVisible(false);
        panelVerEnvaces.setVisible(false);
        panelInicio.setVisible(false);
        panelClientes.setVisible(false);
        panelRegistrarVenta.setVisible(true);
        panelRegistrarVenta.actualizarListaArticulos();


    }//GEN-LAST:event_registroDeVentaPanelMouseClicked

    private void verEnvasesPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_verEnvasesPanelMouseClicked
        // TODO add your handling code here:
        this.tituloLbl.setText("EcoShop");

        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.verEnvasesPanel.setBackground(new Color(51, 153, 0));
        panelRegistrarArticulo.setVisible(false);
        panelRegistrarVenta.setVisible(false);
        panelPuntosDeVenta.setVisible(false);
        panelReportes.setVisible(false);
        panelInicio.setVisible(false);
        panelClientes.setVisible(false);
        panelVerEnvaces.setVisible(true);
    }//GEN-LAST:event_verEnvasesPanelMouseClicked

    private void inicioPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inicioPanelMouseClicked
        this.tituloLbl.setText("Bienvenido");

        // TODO add your handling code here:
        this.inicioPanel.setBackground(new Color(51, 153, 0));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51, 118, 18));
        panelRegistrarArticulo.setVisible(false);
        panelRegistrarVenta.setVisible(false);
        panelPuntosDeVenta.setVisible(false);
        panelReportes.setVisible(false);
        panelVerEnvaces.setVisible(false);
        panelClientes.setVisible(false);
        panelInicio.setVisible(true);
    }//GEN-LAST:event_inicioPanelMouseClicked

    private void listaClientesPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaClientesPanelMouseClicked
        // TODO add your handling code here:
        this.inicioPanel.setBackground(new Color(51, 118, 18));
        this.puntoDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.registrarArticuloPanel.setBackground(new Color(51, 118, 18));
        this.registroDeVentaPanel.setBackground(new Color(51, 118, 18));
        this.reportesEstadisticasPanel.setBackground(new Color(51, 118, 18));
        this.verEnvasesPanel.setBackground(new Color(51, 118, 18));
        this.listaClientesPanel.setBackground(new Color(51, 153, 0));
        panelRegistrarArticulo.setVisible(false);
        panelRegistrarVenta.setVisible(false);
        panelPuntosDeVenta.setVisible(false);
        panelReportes.setVisible(false);
        panelVerEnvaces.setVisible(false);
        panelInicio.setVisible(false);
        panelClientes.iniciarListaClientes();
        panelClientes.actualizarListaCliente();
        panelClientes.setVisible(true);
    }//GEN-LAST:event_listaClientesPanelMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel backgroundPanel;
    private javax.swing.JLabel envasesLbl;
    private javax.swing.JLabel envasesTxtLbl;
    private javax.swing.JLabel inicioIconoLbl;
    private javax.swing.JPanel inicioPanel;
    private javax.swing.JLabel inicioTxtLbl;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel listaClientesLbl;
    private javax.swing.JPanel listaClientesPanel;
    private javax.swing.JLabel listaClientesTxtLbl;
    private javax.swing.JPanel panelCentral;
    private javax.swing.JLabel puntoDeVentaLbl;
    private javax.swing.JPanel puntoDeVentaPanel;
    private javax.swing.JLabel puntoDeVentaTxtLbl;
    private javax.swing.JLabel registrarArticuloIconoLbl;
    private javax.swing.JPanel registrarArticuloPanel;
    private javax.swing.JLabel registrarArticuloTxtLbl;
    private javax.swing.JPanel registroDeVentaPanel;
    private javax.swing.JLabel reporteLbl;
    private javax.swing.JPanel reportesEstadisticasPanel;
    private javax.swing.JLabel reportesTxtLbl;
    private javax.swing.JPanel sidepanePanel;
    private javax.swing.JLabel tituloLbl;
    private javax.swing.JPanel tituloPanel;
    private javax.swing.JLabel tituloSpLbl;
    private javax.swing.JLabel ventasLbl;
    private javax.swing.JLabel ventasTxtLbl;
    private javax.swing.JPanel verEnvasesPanel;
    // End of variables declaration//GEN-END:variables

}
