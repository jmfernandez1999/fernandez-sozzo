/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import dominio.Articulo;
import dominio.Sistema;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jmfer
 */
public class PanelReportesEstadisticas extends javax.swing.JPanel {

    /**
     * Creates new form PanelReportesEstadisticas
     */
    private Sistema s;
    DefaultTableModel modeloTablaArticulos;
    DefaultTableModel modeloTablaMeses;

    ;

    public PanelReportesEstadisticas(Sistema sis) {
        s = sis;
        initComponents();
        actualizarTabla();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVentas = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        labelTotalVentas = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaVentasPorMes = new javax.swing.JTable();
        lablel$ = new javax.swing.JLabel();
        labelC = new javax.swing.JLabel();
        lablelBenefcio = new javax.swing.JLabel();
        labelbenefcioC02 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Estadísticas");

        tablaVentas.setAutoCreateRowSorter(true);
        tablaVentas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo Articulo", "Nombre Articulo", "Cantidad de Ventas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tablaVentas.setEditingColumn(1);
        tablaVentas.setEditingRow(1);
        tablaVentas.setGridColor(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(tablaVentas);

        jLabel2.setText("Total de ventas de la tienda:");

        labelTotalVentas.setText("0");

        tablaVentasPorMes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mes", "Cantidad De Ventas"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tablaVentasPorMes);

        lablel$.setText("Estimación de beneficio en $:");

        labelC.setText("Estimación de beneficio en C02:");
        labelC.setToolTipText("");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/info_17px.png"))); // NOI18N
        jLabel3.setToolTipText("Estimación del beneficio del total de las ventas, en las cuales los clientes presentaron envase.");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/info_17px.png"))); // NOI18N
        jLabel4.setToolTipText("Estimacion del beneficio ahorrado en niveles de emision de C02, por cada peso se no se emiten 0,3 ppm de C02.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 530, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(labelTotalVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lablel$, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lablelBenefcio))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelC)
                        .addGap(18, 18, 18)
                        .addComponent(labelbenefcioC02)))
                .addGap(0, 45, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(364, 364, 364))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(labelTotalVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lablel$)
                                .addComponent(lablelBenefcio))
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelC)
                            .addComponent(labelbenefcioC02)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(36, 36, 36))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void actualizarTabla() {
        
        if (s.getListaVentas().size() == 0) {
            if (this.isShowing()) {
                JOptionPane.showMessageDialog(this, "No hay ventas registradas en el sistema", "Información", INFORMATION_MESSAGE);

            }
        } else {
            this.lablelBenefcio.setText(""+s.estimacionBeneficio());
            this.labelbenefcioC02.setText(""+(s.estimacionBeneficio()*0.3)+"PPM");
            int[] stats = s.articuloMasVendido();
            String[] col = {"Articulo", "Nombre", "Cantidad vendida"};
            DefaultTableModel x = new DefaultTableModel(0, 3);
            x.setColumnIdentifiers(col);
            this.tablaVentas.setModel(x);
            DefaultTableModel model = (DefaultTableModel) this.tablaVentas.getModel();

            for (int i = 1; i < stats.length; i++) {
                Articulo a = s.getListaArticulos().get(i - 1);
                model.addRow(new Object[]{a.getCodigo(), a.getNombre(), stats[i]});
            }
            this.tablaVentas.setModel(model);

            String[] columnas = {"Mes", "Cantidad vendida"};
            DefaultTableModel z = new DefaultTableModel(0, 2);
            z.setColumnIdentifiers(columnas);

            this.tablaVentasPorMes.setModel(z);
            this.labelTotalVentas.setText("" + stats[0]);
            int[] cantidadDeVentasXmes = s.cantidadDeVentasMes();
            modeloTablaMeses = (DefaultTableModel) this.tablaVentasPorMes.getModel();

            String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
                "Septiembre", "Octubre", "Noviembre", "Diciembre"};
            for (int i = 1; i < cantidadDeVentasXmes.length; i++) {
                modeloTablaMeses.addRow(new Object[]{meses[i - 1], cantidadDeVentasXmes[i]});
            }

            
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelC;
    private javax.swing.JLabel labelTotalVentas;
    private javax.swing.JLabel labelbenefcioC02;
    private javax.swing.JLabel lablel$;
    private javax.swing.JLabel lablelBenefcio;
    private javax.swing.JTable tablaVentas;
    private javax.swing.JTable tablaVentasPorMes;
    // End of variables declaration//GEN-END:variables

}
