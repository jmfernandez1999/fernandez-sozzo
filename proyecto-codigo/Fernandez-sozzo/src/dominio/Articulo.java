package dominio;

import java.io.Serializable;

public class Articulo implements Serializable  {

    private String nombre;
    private float precio;
    private String material;
    private String codigo;
    private String OrigenMateriaPrima;
    private String tipo;

    public Articulo(String nombre, float precio, String codigo, String material, String origenMateriaPrima, String tipo) {
        this.precio = precio;
        this.codigo = codigo;
        this.OrigenMateriaPrima = origenMateriaPrima;
        this.material = material;
        this.tipo = tipo;
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String Nombre) {
        this.nombre = Nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getOrigenMateriaPrima() {
        return OrigenMateriaPrima;
    }

    public void setOrigenMateriaPrima(String OrigenMateriaPrima) {
        this.OrigenMateriaPrima = OrigenMateriaPrima;
    }

    @Override
    public String toString() {
        return   codigo + " -"  + nombre + "     Tipo:" + tipo + " Precio: "+precio;
    }

}
