package dominio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author jmfer
 */
public class Sistema {

    private ArrayList<Articulo> listaArticulos = new ArrayList<Articulo>();
    private ArrayList<Venta> listaVentas = new ArrayList<Venta>();

    public Sistema() {
        this.listaVentas = new ArrayList<Venta>();
        this.listaArticulos = new ArrayList<Articulo>();
    }

    public ArrayList<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    

    public void agregarArticulo(Articulo unArticulo) {
        listaArticulos.add(unArticulo);
        

    }

    public ArrayList<Venta> getListaVentas() {
        return listaVentas;
    }

    

    public void agregarVenta(Venta unaVenta) {
        listaVentas.add(unaVenta);
    }


    public int[] articuloMasVendido() {

        if (this.getListaArticulos().size() > 0) {
            int[] listaMasVendidos = new int[this.getListaArticulos().size() + 1];
            for (int i = 0; i < this.getListaVentas().size(); i++) {
                Venta v = this.getListaVentas().get(i);
                for (int j = 0; j < v.getListaArticulos().size(); j++) {
                    Articulo a = v.getListaArticulos().get(j);

                    String[] splited = a.getCodigo().split("-");

                    int pos = Integer.parseInt(splited[1]);
                    listaMasVendidos[pos] += 1;
                    listaMasVendidos[0] += 1;
                }

            }


           
            int max = 0;
            int pos = 0;
            for (int i = 0; i < listaMasVendidos.length; i++) {

                if (listaMasVendidos[i] > max) {
                    max = listaMasVendidos[i];
                    pos = i;
                }

            }
            return listaMasVendidos;
        } else {
            int[] vacio = {};
            return vacio;
        }
    }

    public int[] cantidadDeVentasMes() {
        int[] cantidadVentasMeses = new int[13];
        for (int i = 0; i < this.getListaVentas().size(); i++) {
            Venta v = this.getListaVentas().get(i);
            String fecha = v.getFechaVenta();
            String[] splited = fecha.split("-");
            int mes = Integer.parseInt(splited[0].split("/")[1]);
            cantidadVentasMeses[mes] += 1;
            cantidadVentasMeses[0]+=1;
        }
        return cantidadVentasMeses;
    }
    
    public int estimacionBeneficio(){
        int beneficio=0;
        for (int i = 0; i < this.getListaVentas().size(); i++) {
            Venta v = this.getListaVentas().get(i);
            if(v.getDescuento()!=0){
                beneficio +=v.getDescuento();
            }
            
        }
        System.out.println("beneficio:"+ beneficio);
        return beneficio;
    }

    public void guardarArticulos() throws FileNotFoundException, IOException {
        File f = new File("Articulos.txt");
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        for (int i = 0; i < this.getListaArticulos().size(); i++) {
            Articulo a = this.getListaArticulos().get(i);
            oos.writeObject(a);
        }
        oos.close();
        fos.close();
    }

    public void abrirArticulos() throws FileNotFoundException {
        File f = new File("Articulos.txt");
        ObjectInputStream ois;
        boolean continuar = true;
        try {
            FileInputStream fis = new FileInputStream(f);
            fis = new FileInputStream("Articulos.txt");
            ois = new ObjectInputStream(fis);
            while (continuar) {
                Articulo a = null;
                try {
                    a = (Articulo) ois.readObject();
                } catch (ClassNotFoundException e) {
                    //  System.out.println("ClassNotFoundException");
                }
                if (a != null) {
                    this.agregarArticulo(a);
                } else {
                    continuar = false;
                }
            }
        } catch (FileNotFoundException e) {

            // System.out.println("FileNotFoundException");
        } catch (IOException e) {

            // System.out.println("IOException");
        }

    }

    public void guardarVentas() throws FileNotFoundException, IOException {
        File f = new File("Ventas.txt");
        FileOutputStream fos = new FileOutputStream(f);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        for (int i = 0; i < this.getListaVentas().size(); i++) {
            Venta v = this.getListaVentas().get(i);
            oos.writeObject(v);
        }
        oos.close();
        fos.close();
    }

    public void abrirVentas() throws FileNotFoundException {
        File f = new File("Ventas.txt");
        ObjectInputStream ois;
        boolean continuar = true;
        try {
            FileInputStream fis = new FileInputStream(f);
            fis = new FileInputStream("Ventas.txt");
            ois = new ObjectInputStream(fis);
            while (continuar) {
                Venta v = null;
                try {
                    v = (Venta) ois.readObject();
                } catch (ClassNotFoundException e) {
                    //  System.out.println("ClassNotFoundException");
                }
                if (v != null) {
                    this.agregarVenta(v);
                } else {
                    continuar = false;
                }
            }
        } catch (FileNotFoundException e) {

            // System.out.println("FileNotFoundException");
        } catch (IOException e) {

            // System.out.println("IOException");
        }

    }
}
