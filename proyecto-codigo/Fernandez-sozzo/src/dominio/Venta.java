package dominio;

import java.io.Serializable;
import java.util.ArrayList;

public class Venta implements Serializable {

    private ArrayList<Articulo> listaArticulos;
    private String fechaVenta;
    private String formaDePago;
    private String numeroDeSerie;
    private float precioTotal;
    private boolean trajoEnvase;
    private  int descuento;
    private String idCliente;

    public Venta(ArrayList<Articulo> listaArticulos, String fechaVenta, String formaDePago, String numeroDeSerie, float precioTotal,int descuento,boolean trajoEnvase,String idCliente) {
        this.listaArticulos = listaArticulos;
        this.fechaVenta = fechaVenta;
        this.formaDePago = formaDePago;
        this.numeroDeSerie = numeroDeSerie;
        this.precioTotal = precioTotal;
        this.descuento=descuento;
        this.trajoEnvase=trajoEnvase;
        this.idCliente=idCliente;
    }

    public boolean isTrajoEnvase() {
        return trajoEnvase;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public void setTrajoEnvase(boolean trajoEnvase) {
        this.trajoEnvase = trajoEnvase;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }
    
    

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    public ArrayList<Articulo> getListaArticulos() {
        return listaArticulos;
    }

    public void setListaArticulos(ArrayList<Articulo> listaArticulos) {
        this.listaArticulos = listaArticulos;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

   

    public String getFormaDePago() {
        return formaDePago;
    }

    public void setFormaDePago(String formaDePago) {
        this.formaDePago = formaDePago;
    }

    public String getNumeroDeSerie() {
        return numeroDeSerie;
    }

    public void setNumeroDeSerie(String numeroDeSerie) {
        this.numeroDeSerie = numeroDeSerie;
    }

   

    @Override
    public String toString() {
        return "Venta{" + "articulos=" + listaArticulos + ", fechaVenta=" + fechaVenta + ", cantidad=" + ", formaDePago=" + formaDePago + ", numeroDeSerie=" + numeroDeSerie + "precioT=" + precioTotal + '}';
    }

}
